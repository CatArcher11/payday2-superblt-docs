# Automatic updates

In vanilla BLT, without a large amount of effort on your part, it's not possible to use
BLT's automatic updates system if your mod is not hosted on paydaymods.com, especially
when the mod is disabled.

SuperBLT adds a way to make your mods update themselves using customizable endpoints, fixing
this issue.

In your mod.txt file, the update tag now takes custom URLs:
```
"updates" : [
	{
		"identifier" : "anythingyouwant",
		"host": {
			"meta": "https://example.com/my_meta.json",
			"download": "https://example.com/download.zip",
			"patchnotes": "https://example.com/patchnotes.html"
		}
	}
]
```

The identifier can be anything you like - issues will arise if multiple mods have the same
identifier, so set it to something that you can reasonably assume no other mod will use.

The `meta` file should be in the following format:
```
[
	{
		"ident": "anythingyouwant",
		"hash": "43e7cd36567c755be88e60bde45ba418527c692af982d45fbedb8b5a8c792772",
		"patchnotes_url": "https://example.com/other-patchnotes.html",
		"download_url": "https://example.com/other-download.zip"
	}
]
```

The identifier in the meta must be the same as that in your update tag. The hash value is the
hash of the newest version of your mod, which is used to determine if the mod is up to date or
not (more details about this, such as how to find the hash, will go here when I get around to it).

TODO: Add documentation for the version entry, which can (and now generally should) be used
instead of the hash entry.

The `patchnotes_url` and `download_url` values are optional. If they are supplied, they override
the `pathnotes` and `download` values supplied in `mod.txt`. In fact, you can actually omit the
`download` and `patchnotes` values from `mod.txt` and only supply them in the update meta - that way,
if you don't have some kind of redirect system set up, you can have users update with a new archive file
for each update (do note that you'll have trouble if you don't supply them in either).

